var DataSource = function($http, configArg) {
	var defaultConfig = {
			url: ""
	};
	var config = angular.extend({}, defaultConfig, configArg);
	
	this.get = function(params, callback) {
		$http.get(config.url, {
			params: { offset: params.offset , count: params.count }
		}).success(function(result) {
			callback(result.items);
		});
	};
};
