var controllers = (function() {
	var createListController = function(url) {
		return ['$http', '$scope', '$location', '$routeParams', function($http, $scope, $location, $routeParams) {
			$scope.page = $location.search().page;
			$scope.$on('pageChange', function(event, page) {
				$location.path($location.path(), angular.extend($location.search(), {
					page: page
				}));
			});
			$scope.dataSource = new DataSource($http, {
				url: url
			});
		}];
	};

	var generateCrudController = function(config) {
		return ['$scope', '$injector', function($scope, $injector) {
			var serviceName = config.serviceName;
			var setModel = function(model) {
				$scope.entityId = model.id;
				$scope[config.model] = model;
			};
			var getModel = function() {
				return $scope[config.model];
			};
			setModel({});

			$injector.get(serviceName).then(function(service) {
				$scope.save = function() {
					var model = getModel();
					(model.id ? service.update : service.create).call(service, {
						entity: model
					}).then(function(savedEntity) {
						setModel(angular.copy(savedEntity));
						$scope.$emit('entitySaved', savedEntity);
					}, function(errors) {
						errors.forEach(function(e) { e.code = 'server_' + e.code; });
						$scope.$broadcast('validationErrors',errors);
					});
				};
				
				$scope.load = function() {
					service.getFromParam({ id: $scope.entityId }).then(function(entity) {
						setModel(entity);
					});
				};
				
				$scope.$watch('entityId', function(newVal) {
					if (getModel().id != newVal) {
						$scope.load();
					};
				});

				$scope.$on('saveRequest', function() {
					$scope.save();
				});
			});
		}];
	};
	
	var entityManagementPageController = ['$scope', '$routeParams', '$location', function($scope, $routeParams, $location) {
		$scope.entityId = $routeParams.id;
		$scope.$on('entitySaved', function(event, entity) {
			var path = $location.path();
			var finalPath = path.replace('create', entity.id);
			$location.path(finalPath);
		});
	}];

	var extend = function(controller, otherController) {
		return ['$injector', '$scope', function($injector, $scope) {
			$injector.invoke(controller, this, {
				$scope: $scope
			});
			$injector.invoke(otherController, this, {
				$scope: $scope
			});
		}];
	};

	var basicItemController = generateCrudController({
		model: 'item',
		serviceName: 'itemService'
	});

	var ItemFormController = extend(extend(basicItemController, ['$scope', 'userService', function($scope, servicePromise) {
		servicePromise.then(function(service) {
			service.search({
				query: "select u from User u"
			}).then(function(result) {
				$scope.users = result.items;
			});
		});
	}]), entityManagementPageController);

	var basicUserController = generateCrudController({
		model: 'user',
		serviceName: 'userService'
	});

	var UserFormCtrl = extend(basicUserController, entityManagementPageController);

	var basicBidController = generateCrudController({
		model: 'bid',
		serviceName: 'bidService'
	});
	
	var basicBidController = extend(basicBidController, entityManagementPageController);
	
	var BidFormCtrl = extend(basicBidController, ['$scope', 'userService', 'itemService', function($scope, userServicePromise, itemServicePromise) {
		userServicePromise.then(function(service) {
			service.search({
				query: "select u from User u"
			}).then(function(searchResult) {
				$scope.users = searchResult.items;
			});
		});
		
		itemServicePromise.then(function(service) {
			service.search({
				query: "select i from Item i"
			}).then(function(searchResult) {
				$scope.items = searchResult.items;
			});
		});
	}]);

	return {

		ItemFormCtrl: ItemFormController,

		UserFormCtrl: UserFormCtrl,
		
		BidFormCtrl: BidFormCtrl,

		usersCtrl: createListController('../user/search'),

		itemsCtrl: createListController('../item/search'),

		bidsCtrl: createListController('../bid/search'),

		gridCtrl: [
				'$scope',
				function($scope) {
					var config = {
						pageSize: 3
					};
					$scope.rows = [];

					$scope.goToPage = function(passedPage) {

						var page = parseInt(passedPage) || 1;
						if ((!$scope.canNavigateToPreviousPage() && (page < $scope.page))
								|| (!$scope.canNavigateToNextPage() && (page > $scope.page)))
							return;

						$scope.dataSource.get({
							offset: config.pageSize * (page - 1),
							count: config.pageSize
						}, function(items) {
							$scope.rows = items;
							$scope.page = page;
							$scope.$emit('pageChange', page);
						});
					};

					$scope.nextPage = function() {
						$scope.goToPage($scope.page + 1);
					};

					$scope.previousPage = function() {
						$scope.goToPage($scope.page - 1);
					};

					$scope.canNavigateToPreviousPage = function() {
						return $scope.page > 1;
					};

					$scope.canNavigateToNextPage = function() {
						return $scope.rows.length == config.pageSize;
					};

					var columns = $scope.columns = [];

					this.addColumn = function(column) {
						columns.push(column);
					};

					$scope.goToPage($scope.page || 1);
				}],

		dashboardCtrl: ['$scope', 'rootControllerService', function($scope, rootControllerService) {
			rootControllerService.then(function(service) {
				$scope.checkIfCanAddSampleData = function() {
					service.checkSampleData().then(function(result) {
						$scope.canAddSampleData = result == 'true';
					});
				};

				$scope.addSampleData = function() {
					service.generateSampleData().then($scope.checkIfCanAddSampleData);
				};

				$scope.canAddSampleData = false;
				$scope.checkIfCanAddSampleData();
			});
		}]
	};
})();
