'use strict';

/* Directives */

(function initDirectives() {
	var app = angular.module('blackStar.directives');

	app.directive('uiGrid', function() { // Grid directive
		return {
			restrict: 'EA',
			transclude: true,
			templateUrl: '../app/partials/grid.html',
			scope: {
				dataSource: '=source',
				entityName: '@',
				page: '='
			},
			controller: controllers.gridCtrl
		};
	});

	app.directive('column', function() {
		return {
			restrict: 'EA',
			require: '^uiGrid',
			link: function(scope, element, attrs, uiGridCtrl) {
				uiGridCtrl.addColumn({
					name: attrs.name,
					sortable: scope.$eval(attrs.sortable),
					asLink: scope.$eval(attrs.asLink)
				});
			}
		};
	});

	app.directive('cell', function() {
		return {
			restrict: 'E',
			scope: {
				data: '=',
				ref: '=link'
			},
			require: '^uiGrid',
			templateUrl: '../app/partials/cell.html',
			link: function(scope, element, attrs, uiGridCtrl) {
				if (scope.ref == null) {
					scope.displayData = true;
					scope.displayLink = false;
				} else {
					scope.displayData = false;
					scope.displayLink = true;

					// XXX: ugly HACK!
					scope.link = window.location.pathname + '/' + scope.ref;
				}
			}
		};
	});

	app.directive('uniqueEmail', ['emailValidator', function(validator) {
		return {
			require: 'ngModel',
			scope: {
				url: '@',
				entityClass: '@uniqueEmail'
			},
			link: function(scope, elm, attrs, ctrl) {
				ctrl.$parsers.unshift(function(viewValue) {
					validator.check(viewValue).then(
							function(value) { ctrl.$setValidity('uniqueEmail', true); },
							function(error) { ctrl.$setValidity('uniqueEmail', false); }
					);
					return viewValue;
				});
			}
		};
	}]);
	
	app.directive('bsform',['$injector', function($injector) {
		return {
			scope: {
				model: '@'
			},
			restrict: 'E',
			transclude: true,
			templateUrl: '../app/partials/directives/bsform.html',
			controller: ['$scope', function($scope) {
				var setModel = function(newModel) {
					$scope.$parent[$scope.model] = newModel;
				};
				
				var getModel = function() {
					return $scope.$parent[$scope.model];
				};
				
				var backupEntity = {};
				var serversideValidationErrors = [];
				
				var toggleError = function(error, flag) {
					var targetForm = error.field ? $scope.form[error.field] : $scope.form;
					targetForm.$setValidity(error.code, !flag);
				};
				
				$scope.$on('validationErrors', function(event, externalValidationErrors) {
					for (var i in externalValidationErrors) {
						var error = externalValidationErrors[i];
						toggleError(error, true);
						serversideValidationErrors.push(error);
					}
				});

				$scope.clearServersideValidationErrors = function() {
					var error;
					while(error = serversideValidationErrors.pop()) {
						toggleError(error, false);
					}
				};

				$scope.clearServerSideError = function(field) {
					serversideValidationErrors = serversideValidationErrors.reduce(function(acc, e) {
						if (e.field == field) 
							toggleError(e, false);
						else
							acc = acc.concat(e); 
						return acc;
					}, []);
				};
				
				this.registerServerSideValidationAwareControl = function(path) {
					$scope.$parent.$watch(path, function(newVal) {
						$scope.clearServerSideError(path.substr(path.indexOf('.') + 1));
					});
				};

				$scope.reset = function() {
					$scope.clearServersideValidationErrors();
					setModel(angular.copy(backupEntity));
				};

				$scope.save = function() {
					$scope.clearServersideValidationErrors();
					$scope.$emit('saveRequest');
				};
				
				$scope.remove = function() {
					$scope.$emit('deleteRequest');
				};
			}]
		};
	}]);
	
	app.directive('serverSideValidationAware', [function() {
		return {
			scope: true,
			require: ['ngModel', '^bsform'],
			link: function(scope, elem, attrs, controllers) {
				var bsFormCtrl = controllers[1];
				var model = elem.attr('ng-model');
				bsFormCtrl.registerServerSideValidationAwareControl(model);
			}
		}
	}]);
	
	app.directive('bsformcontrol',[function() {
		return {
			scope: {
				label: '@',
			},
			restrict: 'E',
			transclude: true,
			require: '?^form',
			templateUrl: '../app/partials/directives/bsformcontrol.html',
			link: function(scope, elem, attrs, formCtrl) {
				scope.widgetId = Math.random();
				var content = elem[0].querySelector('.angular-widget-container > *');
				content.id = scope.widgetId;
				content.classList.add('form-control');
				scope.property = content.getAttribute('name');
				if (formCtrl) {
					scope.isValid = function() {
						return formCtrl[scope.property].$valid;
					};
					scope.isDirty = function() {
						return formCtrl[scope.property].$dirty;
					};
					scope.hasServersideErrors = function() {
						var hasServersideErrors = false;
						var errors = formCtrl[scope.property].$error;
						for (var code in errors) {
							if (errors[code] && code.indexOf('server_') != -1) {
								hasServersideErrors = true;
								break;
							}
						}
						return hasServersideErrors;
					};
				}
			}
		};
	}]);
})();
