'use strict';

//Declare app level module which depends on filters, and services
var app = angular.module('blackStar', ['blackStar.directives', 'blackStar.filters', 'blackStar.services', 'ngRoute']);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		$locationProvider.html5Mode(true);
		
		RoutingUtils.generateRoutingEntriesForEntity('user', $routeProvider);
		RoutingUtils.generateRoutingEntriesForEntity('bid', $routeProvider);
		RoutingUtils.generateRoutingEntriesForEntity('item', $routeProvider);
		
		$routeProvider.otherwise({ redirectTo: '/', templateUrl: '../app/partials/dashboard.html', controller: controllers.dashboardCtrl });
	}]);

var bsDirectives = angular.module('blackStar.directives', ['blackStar.services']);
var bsFilters = angular.module('blackStar.filters', []);
var bsServices = angular.module('blackStar.services', []);