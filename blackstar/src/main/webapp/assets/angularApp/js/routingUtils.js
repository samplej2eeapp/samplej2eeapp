var RoutingUtils = {};

RoutingUtils.generateRoutingEntriesForEntity = function(entityName, $routeProvider, config) {
	
	function capitalise(string)
	{
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}
	if (!String.prototype.format) {
		String.prototype.format = function() {
			var args = arguments;
			return this.replace(/{(\d+)}/g, function(match, number) { 
				return typeof args[number] != 'undefined'
					? args[number]
				: match
				;
			});
		};
	}

	config = config || {};
	
	var pluralEntityName = config.pluralEntityName || (entityName + "s");
	
	var baseUrl = '/{0}'.format(pluralEntityName);
	var createEntityUrl = '{0}/create/'.format(baseUrl);
	var getEntityUrl = '{0}/:id'.format(baseUrl);
	
	var baseTemplateUrl = '../app/partials';
	var listTemplateUrl = baseTemplateUrl + '/{0}.html'.format(pluralEntityName);
	var entityFormTemaplateUrl = baseTemplateUrl + '/{0}Form.html'.format(entityName);
	
	var manageEntityControllerName = 'manage' + capitalise(entityName) + 'Ctrl';
	var listControllerName = pluralEntityName + 'Ctrl';

	$routeProvider.when(baseUrl, { templateUrl: listTemplateUrl, controller: controllers[listControllerName], reloadOnSearch: false  });
	$routeProvider.when(createEntityUrl, { templateUrl: entityFormTemaplateUrl, controller: controllers[manageEntityControllerName] });
	$routeProvider.when(getEntityUrl, { templateUrl: entityFormTemaplateUrl, controller: controllers[manageEntityControllerName] });
	
};





		