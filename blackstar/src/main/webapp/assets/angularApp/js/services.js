'use strict';

/* Directives */

(function initDirectives() {
	var app = angular.module('blackStar.services');
	
	app.service('emailValidator', ['$http', '$q', function($http, $q) {
		var validate = function(email) {
			var defer = $q.defer();
			if(!email) {
				defer.resolve();
			} else {
				$http.get(window.basePath + "/user/checkUserEmailCorrectness/", {
					params: {
						emailString: email
					}
				}).success(function() {
					defer.resolve();
				}).error(function() {
					defer.reject();
				});
			}
			return defer.promise;
		};

		return {
			check: validate
		};
	}]);
	
	var defineMethod = function(object, controllerPath, methodDefinition, httpProvider, promiseProvider) {
		var methodName = methodDefinition.name;
		var path = methodDefinition.path;
		var basePath = '../' + controllerPath + '/' + path;
		var argsMapByType = methodDefinition.arguments.reduce(function(acc, e) {
			acc[e.type] = (acc[e.type] || []).concat(e);
			return acc;
		}, {});
		
		object[methodName] = function() {
			var defer = promiseProvider.defer();
			var args = arguments[0];
			var concretePath = basePath;
			
			// Path params
			(argsMapByType.PATH || []).map(function(argDef) {
				concretePath = concretePath.replace('{{0}}'.format(argDef.name), args[argDef.name]);
			});
			
			var params = {};
			
			var handledParamNames = [];
			
			// request params
			var rp = (argsMapByType.PARAMETER || []).reduce(function(acc, argDef) { 
				handledParamNames.push(argDef.name);
				acc[argDef.name] = args[argDef.name]; return acc; 
			}, {});
			angular.extend(params, rp);
			
			if (argsMapByType.MODEL_ATTRIBUTE && argsMapByType.REQUEST_BIDY) {
				throw new Error("Cannot use @RequestBody and @ModelAttrubute at the same time");
			}

			// Request body
			// TODO
			var data = {};
			
			// model attribute
			if (argsMapByType.MODEL_ATTRIBUTE) {
				var ma = Object.keys(args).reduce(function(acc, e) {
					if (handledParamNames.indexOf(e) == -1) {
						var model = args[e];
						for (var key in model) {
							var val = model[key];
							var value = null;
							if (val instanceof Array) {
								value = '';
								for (var i in val) {
									value += val[i] + ',';
								}
							} else {
								value = val;
							}
							acc[key] = value;
						}
					}
					return acc;
				}, {});
				angular.extend(params, ma);
			}
			
			
			httpProvider({
				method: methodDefinition.method || 'GET',
				url: concretePath,
				params: params,
				data: data 
			}).success(function(result) {
				defer.resolve(result);
			}).error(function(result) {
				defer.reject(result);
			});
			return defer.promise;
		};
	};
	
	var defineControllerService = function(url, httpProvider, q) {
		var defer = q.defer();
		var service = {};
		httpProvider.get(url)
			.success(function(controllerDefinition) {
				controllerDefinition.methods.map(function(e) {
					defineMethod(service, controllerDefinition.path, e, httpProvider, q);
				});
				defer.resolve(service);
			});
		return defer.promise;
	};
	
	app.service('userService', ['$http', '$q', function($http, $q) {
		return defineControllerService('../user/introspect', $http, $q);
	}]);
	
	app.service('itemService', ['$http', '$q', function($http, $q) {
		return defineControllerService('../item/introspect', $http, $q);
	}]);
	
	app.service('bidService', ['$http', '$q', function($http, $q) {
		return defineControllerService('../bid/introspect', $http, $q);
	}]);
	
	
	app.service('rootControllerService', ['$http', '$q', function($http, $q) {
		return defineControllerService('../introspect', $http, $q);
	}]);
	
	app.service('sampleDataLoader', ['$q', '$timeout', function($q, $timeout) {
		return {
			load: function() {
				var promise = $q.defer();
				$timeout(function() {
					promise.resolve([1,2,3]);
				}, 3000);
				return promise.promise;
			}
		};
	}]);
	
})();
