package com.epam.blackstar.util;

import java.lang.reflect.Method;

import javax.validation.constraints.NotNull;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

/**
 * Utility methods for working with object proxies.
 */
public class Proxies {

	
	/**
	 * Create class instance. Any method call on this instance will result in throwing {@link IllegalStateException}.
	 * 
	 * @param targeClass class which instance this method returns
	 * @return instance of the given class with overridden methods logic
	 */
	@SuppressWarnings("unchecked")
	@NotNull
	public static <T> T generateNonFunctionalImplementation(Class<T> targeClass) {
		return (T) (Nulls.check(org.springframework.cglib.proxy.Enhancer.create(targeClass, new MethodInterceptor() {

			@Override
			public Object intercept(Object arg0, Method arg1, Object[] arg2, MethodProxy arg3) throws Throwable {
				throw new IllegalStateException("Stub object method call.");
			}

		})));
	}
}
