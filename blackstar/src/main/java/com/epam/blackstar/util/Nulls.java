package com.epam.blackstar.util;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

public class Nulls {

	@NotNull
	public static <T> T or(@Null T a, @NotNull T b) {
		return a != null ? a : b;
	}
	
	@SafeVarargs
	public static <T> Holder<T> any(T ... vals) {
		for (T val : vals) {
			if (val != null) {
				return new Holder<T>(val);
			}
		}
		return new Holder<T>(null);
	}
	
	@NotNull
	public static <T> T check(@Null T a) {
		if (a == null) throw new IllegalArgumentException("Null check failed");
		else return a;
	}
	
	public static class Holder<T> {
		T currentVal;
		
		private Holder(T val) {
			currentVal = val;
		}
		
		public T or(@NotNull T val) {
			return Nulls.or(currentVal, val);
		}
		
		@Null
		public T get() {
			return currentVal;
		}
	}
}
