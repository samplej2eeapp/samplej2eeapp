package com.epam.blackstar.config;

import org.springframework.context.annotation.ComponentScan;

import org.springframework.context.annotation.Configuration;

/**
 * Service layer config.
 */
@Configuration
@ComponentScan(basePackages = { "com.epam.blackstar.services", "com.epam.blackstar.data" })
public class ServicesConfig {
}
