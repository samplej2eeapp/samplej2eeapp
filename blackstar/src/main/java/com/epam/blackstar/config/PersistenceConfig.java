package com.epam.blackstar.config;

import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Persistence config.
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "com.epam.blackstar.data" })
public class PersistenceConfig {

	@Bean
	public EntityManagerFactory getEMF(AppConfiguration config) throws NamingException {
		try {
			return (EntityManagerFactory) new InitialContext().lookup(config.getJndiEmfLocation());
		} catch (NameNotFoundException e) {
			throw new ConfigurationException("Cannot find EntityManagerFactory using JNDI.", e);
		}
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		// no need in JTA yet
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
}
