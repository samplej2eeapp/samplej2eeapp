package com.epam.blackstar.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Spring MVC config. Registers message converters and static resource URLs.
 */

@EnableWebMvc
@ComponentScan(basePackages = { "com.epam.blackstar.web" })
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.addAll(getMessageConverters());
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/assets/**").addResourceLocations("/assets/**");
		registry.addResourceHandler("/app/**").addResourceLocations("/assets/angularApp/**");
	}

	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/");
		return resolver;
	}
	
	private List<HttpMessageConverter<?>> getMessageConverters() {
		HttpMessageConverter<?>[] converters = new HttpMessageConverter<?>[] {
				new org.springframework.http.converter.json.MappingJackson2HttpMessageConverter(),
				new org.springframework.http.converter.xml.MarshallingHttpMessageConverter(new Jaxb2Marshaller())
		};
		return Arrays.<HttpMessageConverter<?>>asList(converters);
	}
}
