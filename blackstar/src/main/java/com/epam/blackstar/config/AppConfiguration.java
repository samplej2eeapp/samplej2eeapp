package com.epam.blackstar.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import com.epam.blackstar.util.Nulls;

@Component
public class AppConfiguration {

	@NotNull
	private static final String DEFAULT_CONFIG_LOCATION = "config.yaml";
	private static final String PROPECONFIG_PROP_NAME = "blackstarConfig";
	
	private Map<String, Object> config;

	@SuppressWarnings("unchecked")
	public AppConfiguration() {

		try {
			String configLocation = Nulls.any(System.getProperty(PROPECONFIG_PROP_NAME), System.getenv(PROPECONFIG_PROP_NAME))
					.or(DEFAULT_CONFIG_LOCATION);
			Reader configReader = new FileReader(new File(configLocation));
			Yaml parser = new Yaml();
			this.config = (Map<String, Object>) parser.load(configReader);
		} catch (FileNotFoundException e) {
			// TODO
		}

	}

	@NotNull
	public String getJndiEmfLocation() {
		return (String) Nulls.or(getPath("config.persistence.emfJndi"), "java:comp/env/persistence/hello-world");
	}

	@SuppressWarnings("unchecked")
	private <T> T getPath(String path) {
		Iterator<String> iterator = Arrays.asList(path.split("\\.")).iterator();
		Map<String, Object> map = config;
		while (iterator.hasNext() && map != null) {
			String token = iterator.next();
			if (iterator.hasNext()) {
				map = (Map<String, Object>) map.get(token);
			} else if (map != null) {
				return (T) map.get(token);
			}
		}
		return null;
	}
}
