package com.epam.blackstar.config;

/**
 * This exception is thrown to indicate that something is wrong with application configuration.
 */
public class ConfigurationException extends RuntimeException {
	
	private static final long serialVersionUID = 1791559206573074187L;

	public ConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}
}
