package com.epam.blackstar.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Initial application config. Sets up sub-configs. Relies on Java EE 6 capabilities.
 */
@Configuration
@ComponentScan(basePackages = { "com.epam.blackstar.config" })
public class SpringBootstrap implements WebApplicationInitializer {

	public void onStartup(ServletContext container) {
		// Detect whether web.xml configuration already performed.
		if (container.getServletRegistration("dispatcher") == null) {
			initApp(container);
		}
	}

	private void initApp(ServletContext container) {
		container.addFilter("httpMethodFilter", HiddenHttpMethodFilter.class);
		AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
		appContext.setServletContext(container);
		appContext.scan(this.getClass().getPackage().getName());
		appContext.refresh();
		ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", new DispatcherServlet(appContext));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/*");
	}
}