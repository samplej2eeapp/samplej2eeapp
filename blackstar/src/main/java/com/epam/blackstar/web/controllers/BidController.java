package com.epam.blackstar.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.blackstar.services.BidService;
import com.epam.blackstar.services.transfer.BidTO;
import com.epam.blackstar.services.transfer.impl.BidTOImpl;

@Controller
@RequestMapping("bid")
public class BidController extends AbstractCrudController<BidTOImpl, BidTO, Long> {

	@Autowired
	public BidController(BidService service) {
		super(service);
	}

}
