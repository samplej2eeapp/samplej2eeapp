package com.epam.blackstar.web.controllers.introspection;

import javax.validation.constraints.NotNull;

public class ArgumentDefinition {
	
	public Class<?> getArgumentClass() {
		return argumentClass;
	}

	public ArgumentType getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	enum ArgumentType {
		PATH,
		PARAMETER,
		MODEL_ATTRIBUTE,
		REQUEST_BODY,
		OTHER
	}
	
	@NotNull
	private Class<?> argumentClass;
	
	@NotNull
	private ArgumentType type;
	
	@NotNull
	private String name;

	public ArgumentDefinition(
			@NotNull String name,
			@NotNull ArgumentType type, 
			@NotNull Class<?> argumentClass) {
		this.type = type;
		this.name = name;
		this.argumentClass = argumentClass;
	}
}
