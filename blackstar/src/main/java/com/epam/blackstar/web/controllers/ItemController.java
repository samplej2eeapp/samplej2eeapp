package com.epam.blackstar.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.blackstar.services.ItemService;
import com.epam.blackstar.services.transfer.ItemTO;
import com.epam.blackstar.services.transfer.impl.ItemTOImpl;

@Controller
@RequestMapping(value = "item")
public class ItemController extends AbstractCrudController<ItemTOImpl, ItemTO, Long>{
	
	@Autowired
	public ItemController(ItemService service) {
		super(service);
	}

}
