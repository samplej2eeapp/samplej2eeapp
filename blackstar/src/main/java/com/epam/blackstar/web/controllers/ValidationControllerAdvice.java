package com.epam.blackstar.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.blackstar.data.validation.GlobalValidator;

/**
 * This class provides global helper methods available in all {@link Controller} classes.
 */
@ControllerAdvice
public class ValidationControllerAdvice {
	
	private GlobalValidator validator;

	@Autowired
	public ValidationControllerAdvice(GlobalValidator globalValidator) {
		this.validator = globalValidator;
	}

	/**
	 * Return serialized validation errors.
	 */
	@ResponseBody
	@ExceptionHandler(value=BindException.class)
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	public List<ObjectError> handleBindingException(BindException e) {
		return e.getAllErrors();
	}
	
	/**
	 * Setup {@link GlobalValidator} for all controllers.
	 */
	@InitBinder
	public void configureGlobalValidator(WebDataBinder binder) {
		// TODO: use JSR 303 validation
		binder.setValidator(validator);
	}

}
