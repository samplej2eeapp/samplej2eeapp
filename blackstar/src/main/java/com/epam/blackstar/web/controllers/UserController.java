package com.epam.blackstar.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epam.blackstar.services.UserService;
import com.epam.blackstar.services.transfer.UserTO;
import com.epam.blackstar.services.transfer.impl.UserTOImpl;

@Controller
@RequestMapping(value = "user")
public class UserController extends AbstractCrudController<UserTOImpl, UserTO, Long>{
	
	@Autowired
	public UserController(UserService service) {
		super(service);
	}
	
	
	@RequestMapping("checkUserEmailCorrectness")
	@ResponseBody
	public boolean checkUserEmailCorrectness(@RequestParam("email") String emailString) {
		if (emailString.startsWith("A")) {
			return true;
		} else if (emailString.startsWith("B")) {
			throw new IllegalArgumentException("email is not unique");
		} else {
			throw new IllegalArgumentException("invalid email");
		}

	}

}