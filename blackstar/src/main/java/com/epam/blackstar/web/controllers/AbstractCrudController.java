package com.epam.blackstar.web.controllers;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epam.blackstar.data.beans.HasId;
import com.epam.blackstar.services.CrudService;
import com.epam.blackstar.services.SearchRequest;
import com.epam.blackstar.services.SearchResult;
import com.epam.blackstar.services.impl.GenericSearchRequest;
import com.epam.blackstar.util.Nulls;

/**
 * Abstract CRUD controller.
 * 
 * @param <ControllerType> this type serves as controller input data. Cannot be interface because of how Spring manages
 *            parameters with {@link ModelAttribute} annotations.
 * @param <ServiceType> this type is used to communicate with CRUD service.
 * @param <EntityTOId> type of entity id.
 */
public abstract class AbstractCrudController<ControllerType extends ServiceType, ServiceType extends HasId<EntityTOId>, EntityTOId> 
		extends AbstractIntrospectableController {
	
	private CrudService<ServiceType, EntityTOId> entityService;

	protected AbstractCrudController(CrudService<ServiceType, EntityTOId> service) {
		this.entityService = service;
	}

	@RequestMapping(value = "create")
	@ResponseBody
	public ServiceType create(@ModelAttribute @Valid @NotNull ControllerType newEntityTO) {
		EntityTOId entityId = entityService.create(newEntityTO);
		return entityService.get(entityId);
	}

	@RequestMapping(value = "get")
	@ResponseBody
	public ServiceType getFromParam(@RequestParam("id") @NotNull EntityTOId id) {
		return get(id);
	}
	
	@RequestMapping(value = "{id}")
	@ResponseBody
	public ServiceType getFromPath(@PathVariable("id") @NotNull EntityTOId id) {
		return get(id);
	}

	@RequestMapping(value = "update")
	@ResponseBody
	public ServiceType update(@ModelAttribute @Valid @NotNull ControllerType editedEntityTO) {
		entityService.update(editedEntityTO);
		return entityService.get(Nulls.check(editedEntityTO.getId()));
	}

	@RequestMapping(value = "delete")
	@ResponseBody
	public void delete(@RequestParam @NotNull ServiceType deletedEntityTO) {
		entityService.delete(deletedEntityTO);
	}

	@RequestMapping("search")
	@ResponseBody
	public SearchResult<ServiceType> search(@ModelAttribute GenericSearchRequest<ControllerType> request) {
		SearchRequest<ServiceType> serviceReques = new GenericSearchRequest<ServiceType>(request.getOffset(), request.getCount());
		return entityService.search(serviceReques);
	}
	
	@RequestMapping("jpqlSearch")
	@ResponseBody
	public SearchResult<ServiceType> search(@RequestParam("query") String searchString) {
		return entityService.jpqlSearch(searchString);
	}

	public ServiceType get(@NotNull EntityTOId id) {
		return entityService.get(id);
	}
	
}
