package com.epam.blackstar.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.blackstar.data.SampleDataGenerationHelper;

/**
 * Root controller to handle index page request.
 */
@Controller
public class RootController extends AbstractIntrospectableController {

	private SampleDataGenerationHelper sampleDataHelper;

	@Autowired
	public RootController(SampleDataGenerationHelper sampleDataHelper) {
		this.sampleDataHelper = sampleDataHelper;
	}

	/*
	 * Handles all requests without mappings and returns root page. Adds trailing slash if not present.
	 */
	@RequestMapping({ "/ui/**" })
	public String getRootPage(HttpServletRequest req) {
		if (!req.getRequestURI().endsWith("/")) {
			return "redirect:" + req.getRequestURL() + "/";
		} else {
			return getRootView();
		}
	}
	
	@RequestMapping("/")
	private String getRootRedirection() {
		return "redirect:/ui/";
	}
	
	private String getRootView() {
		return "/app/index.html";
	}

	@RequestMapping("addSampleData")
	@ResponseStatus(HttpStatus.OK)
	public void generateSampleData() {
		sampleDataHelper.generateSampleData();
	}
	
	@RequestMapping("checkIfCanAddSampleData")
	@ResponseBody
	public Boolean checkSampleData() {
		return sampleDataHelper.canAddSampleData();
	}

}
