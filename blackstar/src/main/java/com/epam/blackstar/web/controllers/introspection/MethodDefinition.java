package com.epam.blackstar.web.controllers.introspection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.blackstar.util.Nulls;
import com.epam.blackstar.web.controllers.introspection.ArgumentDefinition.ArgumentType;
import com.google.common.collect.Lists;

public class MethodDefinition {
	
	@NotNull
	private String name = "";
	
	@NotNull
	private String path = "";

	private Collection<ArgumentDefinition> arguments = Lists.newArrayList();

	public MethodDefinition(
			@NotNull String name, 
			@NotNull String path, 
			@NotNull Collection<ArgumentDefinition> arguments) {
		init(name, path, arguments);
	}

	@NotNull
	public String getName() {
		return name;
	}

	@NotNull
	public String getPath() {
		return path;
	}

	@NotNull
	public Collection<ArgumentDefinition> getArguments() {
		return Nulls.check(Collections.unmodifiableCollection(arguments));
	}

	public MethodDefinition(@NotNull Method method) {
		init(getName(method), Nulls.or(getPath(method)[0], "unknown"), getArgumentDefinitions(method));
	}
	
	private void init(
			@NotNull String name, 
			@NotNull String path, 
			@NotNull Collection<ArgumentDefinition> arguments) {
		this.arguments = arguments;
		this.name = name;
		this.path = path;
	}
	
	@NotNull
	private String getName(@NotNull Method targetMethod) {
		return Nulls.check(targetMethod.getName());
	}
	
	@NotNull
	private String[] getPath(@NotNull Method targetMethod) {
		RequestMapping annotation = targetMethod.getAnnotation(RequestMapping.class);
		if (annotation == null) throw new IllegalArgumentException("Target method has no @RequestMapping annotation: " + targetMethod.getName());
		return Nulls.or(annotation.value(), new String[] { "" });
	}

	@NotNull
	private Collection<ArgumentDefinition> getArgumentDefinitions(@NotNull Method targetMethod) {
		List<ArgumentDefinition> arguments = Lists.newLinkedList();

		for (int i = 0; i < targetMethod.getParameterTypes().length; i++) {
			try {
				arguments.add(createArgumentDefinition(
						Nulls.check(targetMethod.getParameterTypes()[i]), 
						Nulls.check(targetMethod.getParameterAnnotations()[i]),
						targetMethod));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return Nulls.check(arguments);
	}
	
	@NotNull
	private ArgumentDefinition createArgumentDefinition(
			@NotNull Class<?> argumentClass, 
			@NotNull Annotation[] annotations,
			@NotNull Method targetMethod) {
		
		ArgumentType argumentType = ArgumentType.OTHER;
		String argumentName = null;
		
		for (Annotation argumentAnnotation : annotations) {
			if (argumentAnnotation instanceof RequestParam) {
				argumentType = ArgumentType.PARAMETER;
				argumentName = ((RequestParam) argumentAnnotation).value();
				break;
			} else if (argumentAnnotation instanceof PathVariable) {
				argumentType = ArgumentType.PATH;
				argumentName = ((PathVariable) argumentAnnotation).value();
				break;
			} else if (argumentAnnotation instanceof ModelAttribute) {
				argumentType = ArgumentType.MODEL_ATTRIBUTE;
				argumentName = ((ModelAttribute) argumentAnnotation).value();
				break;
			} else if (argumentAnnotation instanceof RequestBody) {
				argumentType = ArgumentType.REQUEST_BODY;
				break;
			}
		}
		
		// workaround for eclipse bug: http://www.eclipse.org/forums/index.php/t/518902/
		argumentType = Nulls.check(argumentType);

		ArgumentDefinition definition = new ArgumentDefinition(Nulls.or(argumentName, "unknown"), argumentType, argumentClass);
		return definition;
	}
	
	static boolean canBeHandled(Method method) {
		return method.getAnnotation(RequestMapping.class) != null;
	}
}
