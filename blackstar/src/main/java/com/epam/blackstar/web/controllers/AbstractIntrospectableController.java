package com.epam.blackstar.web.controllers;

import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epam.blackstar.util.Nulls;
import com.epam.blackstar.web.controllers.introspection.ControllerDefinition;

public class AbstractIntrospectableController {
	
	private ControllerDefinition controllerDefinition;
	
	@RequestMapping("introspect")
	@ResponseBody
	@NotNull
	public ControllerDefinition introspect() {
		if (controllerDefinition == null) controllerDefinition = new ControllerDefinition(Nulls.check(this.getClass()));
		
		return Nulls.check(controllerDefinition);
	}
}
