package com.epam.blackstar.web.controllers.introspection;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.blackstar.util.Nulls;
import com.google.common.collect.Lists;

public class ControllerDefinition {
	
	@NotNull
	private String name = "";
	
	@NotNull
	private String path = "";
	
	@NotNull
	public String getName() {
		return name;
	}

	@NotNull
	public String getPath() {
		return path;
	}

	public Collection<MethodDefinition> getMethods() {
		return Collections.unmodifiableCollection(methods);
	}

	private Collection<MethodDefinition> methods = Lists.newArrayList();

	public ControllerDefinition(
			@NotNull String name,
			@NotNull String path,
			@NotNull Collection<MethodDefinition> methods) {
		init(name, path, methods);
	}
	
	public ControllerDefinition(@NotNull Class<?> targetClass) {
		init(getName(targetClass), Nulls.or(getPath(targetClass)[0], "unknown"), collectMethodDefinitions(targetClass));
	}
	
	private void init(
			@NotNull String name,
			@NotNull String path,
			@NotNull Collection<MethodDefinition> methods) {
		this.name = name;
		this.path = path;
		this.methods.addAll(methods);
	}

	@NotNull
	private String getName(Class<?> targetClass) {
		return Nulls.check(targetClass.getName());
	}
	
	@NotNull
	private String[] getPath(Class<?> targetClass) {
		RequestMapping annotation = targetClass.getAnnotation(RequestMapping.class);
		return Nulls.check(annotation == null ? new String[] { "" } : annotation.value());
	}
	
	@NotNull
	private Collection<MethodDefinition> collectMethodDefinitions(Class<?> targetClass) {
		List<MethodDefinition> methodDefinitions = Lists.newLinkedList();
		for (Method method : targetClass.getMethods()) {
			if (MethodDefinition.canBeHandled(method)) {
				try {
					methodDefinitions.add(new MethodDefinition(Nulls.check(method)));			
				} catch (Exception e) {
					// TODO: proper handling 
					e.printStackTrace();
				}
			}
		}
		return Nulls.check(methodDefinitions);
	}
}
