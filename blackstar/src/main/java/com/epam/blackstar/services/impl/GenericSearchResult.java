package com.epam.blackstar.services.impl;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.services.SearchResult;

public class GenericSearchResult<T> implements SearchResult<T> {
	
	@NotNull
	private Collection<T> items;
	
	public GenericSearchResult(@NotNull Collection<T> items) {
		this.items = items;
	}
	@Override
	public Collection<T> getItems() {
		return items;
	}

}
