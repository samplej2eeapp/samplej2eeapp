package com.epam.blackstar.services.impl;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.services.SearchRequest;

public class GenericSearchRequest<T> implements SearchRequest<T> {
	
	@NotNull
	private Long offset = 0L;
	
	@NotNull
	private Long count = 0L;
	
	public GenericSearchRequest() {
	}

	public GenericSearchRequest(@NotNull Long offset, @NotNull Long count) {
		this.offset = offset;
		this.count = count;
	}

	@Override
	public Long getOffset() {
		return offset;
	}

	@Override
	public Long getCount() {
		return count;
	}

	public void setOffset(@NotNull Long offset) {
		this.offset = offset;
	}

	public void setCount(@NotNull Long count) {
		this.count = count;
	}

}
