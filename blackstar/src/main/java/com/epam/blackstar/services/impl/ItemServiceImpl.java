package com.epam.blackstar.services.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.blackstar.data.ItemRepository;
import com.epam.blackstar.data.UserRepository;
import com.epam.blackstar.data.beans.Item;
import com.epam.blackstar.data.beans.User;
import com.epam.blackstar.services.ItemService;
import com.epam.blackstar.services.transfer.ItemTO;
import com.epam.blackstar.services.transfer.impl.ItemTOImpl;
import com.epam.blackstar.util.Nulls;

/**
 * Sample Item service.
 */
@Service
public class ItemServiceImpl extends AbstractCrudService<Item, ItemTO, Long> implements ItemService {

	private ItemRepository itemRepository;
	private UserRepository userRepository;

	@Autowired
	public ItemServiceImpl(ItemRepository itemRepository, UserRepository userRepository) {
		super(itemRepository);
		this.itemRepository = itemRepository;
		this.userRepository = userRepository;
	}

	@Override
	public Collection<ItemTO> getByIds(Collection<Long> ids) {
		return bulkCreateTransferObjectFromJpaObject(itemRepository.getByIds(ids));
	}

	@Override
	@Transactional
	public void assignBuyer(Long itemId, Long buyerId) {
		User buyer = Nulls.check(userRepository.getById(buyerId));
		Item item = Nulls.check(itemRepository.getById(itemId));
		buyer.addItem(item);
	}

	@Override
	protected Item createJpaObjectFromTransferObject(ItemTO transferObject) {
		User seller = Nulls.check(userRepository.getById(transferObject.getSellerId()));
		Item result = new Item(seller);
		result.setId(transferObject.getId());
		result.setName(transferObject.getName());
		result.setDescription(transferObject.getDescription());
		return result;
	}

	@Override
	protected ItemTO createTransferObjectFromJpaObject(Item jpaObject) {
		ItemTO result = new ItemTOImpl();
		result.setId(jpaObject.getId());
		result.setSellerId(Nulls.check(jpaObject.getSeller().getId()));
		result.setName(jpaObject.getName());
		result.setDescription(jpaObject.getDescription());
		return result;
	}
}
