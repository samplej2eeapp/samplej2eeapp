package com.epam.blackstar.services.transfer.impl;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.data.beans.AbstractBean;
import com.epam.blackstar.services.transfer.ItemTO;

public class ItemTOImpl extends AbstractBean<Long> implements ItemTO {

	@NotNull
	private String name = "";
	
	@NotNull
	private String description = "";
	
	@NotNull
	private Long sellerId = getInvalidId();
	
	private Long buyerId;

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public Long getSellerId() {
		return sellerId;
	}

	@Override
	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}

	@Override
	public Long getBuyerId() {
		return buyerId;
	}

	@Override
	public void setBuyerId(Long buyerId) {
		this.buyerId = buyerId;
	}

}
