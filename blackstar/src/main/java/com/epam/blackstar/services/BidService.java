package com.epam.blackstar.services;

import com.epam.blackstar.services.transfer.BidTO;

public interface BidService extends CrudService<BidTO, Long> {
}
