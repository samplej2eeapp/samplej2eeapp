package com.epam.blackstar.services.transfer;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.data.beans.HasId;

public interface ItemTO extends HasId<Long> {

	@NotNull
	String getName();

	void setName(@NotNull String name);

	@NotNull
	String getDescription();

	void setDescription(@NotNull String description);

	@NotNull
	Long getSellerId();

	void setSellerId(@NotNull Long sellerId);

	Long getBuyerId();

	void setBuyerId(Long buyerId);

}