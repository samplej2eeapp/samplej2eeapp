package com.epam.blackstar.services.transfer;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.data.beans.HasId;

public interface UserTO extends HasId<Long> {
	
	@NotNull
	String getEmail();
	
	void setEmail(@NotNull String email);

	@NotNull
	String getLogin();

	void setLogin(@NotNull String login);

	@NotNull
	String getName();

	void setName(@NotNull String name);
	
	@NotNull
	String getPassword();

	void setPassword(@NotNull String password);

	@NotNull
	String getBillingAddress();

	void setBillingAddress(@NotNull String billingAddress);
	
	@NotNull
	Collection<Long> getItemIds();
	
	void setItemIds(@NotNull Collection<Long> itemIds);
	
}