package com.epam.blackstar.services;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.services.transfer.ItemTO;

/**
 * Item service interface.
 * 
 */
public interface ItemService extends CrudService<ItemTO, Long> {
	
	@NotNull
	Collection<? extends ItemTO> getByIds(@NotNull Collection<Long> ids);
	
	void assignBuyer(@NotNull Long itemId,@NotNull Long buyerId);
}