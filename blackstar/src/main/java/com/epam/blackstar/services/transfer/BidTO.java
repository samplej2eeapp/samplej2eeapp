package com.epam.blackstar.services.transfer;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.data.beans.HasId;

public interface BidTO extends HasId<Long> {

	@NotNull
	Long getBidderId();

	void setBidderId(@NotNull Long bidderId);

	@NotNull
	Long getItemId();

	void setItemId(@NotNull Long itemId);

	@NotNull
	Long getAmount();

	void setAmount(@NotNull Long amount);

}