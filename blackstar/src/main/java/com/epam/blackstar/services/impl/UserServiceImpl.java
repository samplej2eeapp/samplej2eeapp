package com.epam.blackstar.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.blackstar.data.UserRepository;
import com.epam.blackstar.data.beans.Item;
import com.epam.blackstar.data.beans.User;
import com.epam.blackstar.services.UserService;
import com.epam.blackstar.services.transfer.UserTO;
import com.epam.blackstar.services.transfer.impl.UserTOImpl;
import com.epam.blackstar.util.Nulls;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;

/**
 * Sample user service.
 */
@Service
public class UserServiceImpl extends AbstractCrudService<User, UserTO, Long> implements UserService {

	@Autowired
	public UserServiceImpl(UserRepository repository) {
		super(repository);
	}

	@Override
	protected User createJpaObjectFromTransferObject(UserTO transferObject) {
		User result = new User();
		result.setName(transferObject.getName());
		result.setLogin(transferObject.getLogin());
		result.setId(transferObject.getId());
		result.setEmail(transferObject.getEmail());
		return result;
	}

	@Override
	protected UserTO createTransferObjectFromJpaObject(User jpaObject) {
		UserTO result = new UserTOImpl();
		result.setId(jpaObject.getId());
		result.setName(jpaObject.getName());
		result.setLogin(jpaObject.getLogin());
		result.setEmail(jpaObject.getEmail());
		result.setItemIds(Nulls.check(Collections2.transform(jpaObject.getItems(), new Function<Item, Long>() {
			public Long apply(Item input) {
				return input.getId();
			}

			;
		})));
		return result;
	}
}
