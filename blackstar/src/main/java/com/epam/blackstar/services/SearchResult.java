package com.epam.blackstar.services;

import java.util.Collection;

import javax.validation.constraints.NotNull;

public interface SearchResult<T> {
	
	@NotNull
	Collection<T> getItems();

}
