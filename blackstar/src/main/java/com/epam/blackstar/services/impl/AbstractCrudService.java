package com.epam.blackstar.services.impl;

import java.util.Collection;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.epam.blackstar.data.Repository;
import com.epam.blackstar.data.beans.HasId;
import com.epam.blackstar.services.CrudService;
import com.epam.blackstar.services.SearchRequest;
import com.epam.blackstar.services.SearchResult;
import com.epam.blackstar.util.Nulls;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;

/**
 * Generic implementation of {@link CrudService} interface.
 * 
 * @param <TransferType> public entity type this service performs CRUD on
 * @param <JpaType> type which repository works with
 * @param <EntityIdType> primary key of TransferType
 */
public abstract class AbstractCrudService<JpaType extends HasId<EntityIdType>, TransferType extends HasId<EntityIdType>, EntityIdType> implements CrudService<TransferType, EntityIdType> {

	private Repository<JpaType, EntityIdType> repository;

	@Autowired
	public AbstractCrudService(Repository<JpaType, EntityIdType> repository) {
		this.repository = repository;
	}

	@Override
	@Transactional
	public EntityIdType create(@NotNull TransferType entity) {
		return repository.create(createJpaObjectFromTransferObject(entity)).getId();
	}

	@Override
	@Transactional
	public void delete(@NotNull TransferType entity) {
		repository.delete(createJpaObjectFromTransferObject(entity));
	}

	@Null
	@Override
	public TransferType get(@NotNull EntityIdType id) {
		JpaType object = repository.getById(id);
		return object == null ? null : createTransferObjectFromJpaObject(object);
	}

	@Override
	@Transactional
	public void update(@NotNull TransferType entity) {
		Assert.notNull(entity.getId());
		
		repository.update(createJpaObjectFromTransferObject(entity));
	}
	
	@Override
	public SearchResult<TransferType> search(SearchRequest<TransferType> query) {
		SearchRequest<JpaType> repositoryQuery = new GenericSearchRequest<JpaType>(query.getOffset(), query.getCount());
		
		SearchResult<JpaType> repositoryResult = repository.search(repositoryQuery);
		Collection<TransferType> items = bulkCreateTransferObjectFromJpaObject(repositoryResult.getItems());
		return new GenericSearchResult<TransferType>(items) {};
	}
	
	@NotNull
	protected final Collection<JpaType> bulkCreateJpaObjectFromTransferObject(@NotNull Collection<TransferType> items) {
		return Nulls.check(Collections2.transform(items, new Function<TransferType, JpaType>() {

			@Override
			public JpaType apply(TransferType input) {
				return createJpaObjectFromTransferObject(Nulls.check(input));
			}
		}));
	}
	
	@NotNull
	protected final Collection<TransferType> bulkCreateTransferObjectFromJpaObject(@NotNull Collection<JpaType> items) {
		return Nulls.check(Collections2.transform(items, new Function<JpaType, TransferType>() {

			@Override
			public TransferType apply(JpaType input) {
				return createTransferObjectFromJpaObject(Nulls.check(input));
			}
		}));
	}
	
	@Override
	public SearchResult<TransferType> jpqlSearch(String searchString) {
		return new GenericSearchResult<>(bulkCreateTransferObjectFromJpaObject(repository.jpqlSearch(searchString).getItems()));
	}
	
	@NotNull 
	abstract protected JpaType createJpaObjectFromTransferObject(@NotNull TransferType transferObject);
	
	@NotNull 
	abstract protected TransferType createTransferObjectFromJpaObject(@NotNull JpaType jpaObject);
}
