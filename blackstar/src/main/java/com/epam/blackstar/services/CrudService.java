package com.epam.blackstar.services;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.data.beans.HasId;

/**
 * Generic CRUD service interface.
 *
 * @param <EntityType> entity this service performs CRUN on
 * @param <EntityIdType> primary key of T
 */
public interface CrudService<EntityType extends HasId<EntityIdType>, EntityIdType> {

	@NotNull
	EntityIdType create(@NotNull EntityType entity);

	void delete(@NotNull EntityType entity);

	EntityType get(@NotNull EntityIdType id);

	void update(@NotNull EntityType entity);
	
	@NotNull
	SearchResult<EntityType> search(SearchRequest<EntityType> query);

	@NotNull
	SearchResult<EntityType> jpqlSearch(String searchString);
}