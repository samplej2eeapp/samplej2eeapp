package com.epam.blackstar.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.blackstar.data.BidRepository;
import com.epam.blackstar.data.ItemRepository;
import com.epam.blackstar.data.UserRepository;
import com.epam.blackstar.data.beans.Bid;
import com.epam.blackstar.data.beans.Item;
import com.epam.blackstar.data.beans.User;
import com.epam.blackstar.services.BidService;
import com.epam.blackstar.services.transfer.BidTO;
import com.epam.blackstar.services.transfer.impl.BidTOImpl;
import com.epam.blackstar.util.Nulls;

@Service
public class BidServiceImpl extends AbstractCrudService<Bid, BidTO, Long> implements BidService {

	private ItemRepository itemRepository;
	private UserRepository userRepository;

	@Autowired
	public BidServiceImpl(BidRepository repository, ItemRepository itemRepository, UserRepository userRepository) {
		super(repository);
		this.itemRepository = itemRepository;
		this.userRepository = userRepository;
	}
	
	@Override
	protected Bid createJpaObjectFromTransferObject(BidTO transferObject) {
		User bidder = userRepository.getById(transferObject.getBidderId());
		Item item = itemRepository.getById(transferObject.getItemId());
		
		// XXX
		Bid result = new Bid(Nulls.check(bidder), Nulls.check(item), transferObject.getAmount());
		result.setId(transferObject.getId());
		return result;
	}

	@Override
	protected BidTO createTransferObjectFromJpaObject(Bid jpaObject) {
		BidTO to = new BidTOImpl();
		to.setAmount(jpaObject.getAmount());
		to.setId(jpaObject.getId());
		
		// XXX
		to.setBidderId(Nulls.check(jpaObject.getBidder().getId()));
		to.setItemId(Nulls.check(jpaObject.getItem().getId()));
		return to;
	}
}
