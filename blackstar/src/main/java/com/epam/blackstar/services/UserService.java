package com.epam.blackstar.services;

import com.epam.blackstar.services.transfer.UserTO;

public interface UserService extends CrudService<UserTO, Long> {
}
