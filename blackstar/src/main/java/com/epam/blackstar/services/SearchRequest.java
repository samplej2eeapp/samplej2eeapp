package com.epam.blackstar.services;

import javax.validation.constraints.NotNull;

public interface SearchRequest<T> {
	
	@NotNull
	Long getOffset();
	
	@NotNull
	Long getCount();
	
}
