package com.epam.blackstar.services.transfer.impl;

import java.util.Collection;
import java.util.Collections;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.data.beans.AbstractBean;
import com.epam.blackstar.services.transfer.UserTO;
import com.epam.blackstar.util.Nulls;

public class UserTOImpl extends AbstractBean<Long> implements UserTO {

	@NotNull
	private String login = "";

	@NotNull
	private String name = "";
	
	@NotNull
	private String email = "";
	
	@NotNull
	private String password = "";
	
	@NotNull
	private String billingAddress = "";

	@NotNull
	private Collection<Long> itemIds = Nulls.check(Collections.<Long> emptySet());

	@Override
	public String getLogin() {
		return login;
	}

	@Override
	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Collection<Long> getItemIds() {
		return itemIds;
	}

	@Override
	public void setItemIds(Collection<Long> itemIds) {
		this.itemIds = itemIds;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public void setPassword(@NotNull String password) {
		this.password = password;
	}

	@Override public String getBillingAddress() {
		return this.billingAddress;
	}

	@Override
	public void setBillingAddress(@NotNull String billingAddress) {
		this.billingAddress = billingAddress;
	}

}
