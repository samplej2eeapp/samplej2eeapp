package com.epam.blackstar.services.transfer.impl;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.data.beans.AbstractBean;
import com.epam.blackstar.services.transfer.BidTO;

public class BidTOImpl extends AbstractBean<Long> implements BidTO {
	
	@NotNull
	private Long bidderId = getInvalidId();
	
	@NotNull
	private Long itemId = getInvalidId();
	
	@NotNull
	private Long amount = getInvalidId();
	

	@Override
	public Long getBidderId() {
		return bidderId;
	}

	@Override
	public void setBidderId(Long bidderId) {
		this.bidderId = bidderId;
	}

	@Override
	public Long getItemId() {
		return itemId;
	}

	@Override
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	@Override
	public Long getAmount() {
		return amount;
	}

	@Override
	public void setAmount(Long amount) {
		this.amount = amount;
	}
}
