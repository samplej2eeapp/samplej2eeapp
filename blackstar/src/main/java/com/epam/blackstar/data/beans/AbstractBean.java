package com.epam.blackstar.data.beans;

import javax.validation.constraints.NotNull;



/**
 * Abstract bean with Id property.
 * 
 * @param <T> type of id
 */
public class AbstractBean<T> implements HasId<T> {

	private T id;

	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}
	
	@NotNull
	public static Long getInvalidId() {
		return -1L;
	}
}
