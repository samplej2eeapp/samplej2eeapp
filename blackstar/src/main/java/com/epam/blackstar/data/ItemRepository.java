package com.epam.blackstar.data;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import com.epam.blackstar.data.beans.Item;

/**
 * Item repository interface.
 *
 */
public interface ItemRepository extends Repository<Item, Long>{

	@NotNull
	public abstract Collection<Item> getByIds(@NotNull Collection<Long> ids);

}