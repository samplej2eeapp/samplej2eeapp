package com.epam.blackstar.data.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.epam.blackstar.data.beans.AbstractJpaBeanWithIdOfTypeLong;
import com.epam.blackstar.services.transfer.BidTO;
import com.epam.blackstar.services.transfer.ItemTO;

public class BidTOValidator implements Validator {
	private Long INVALID_LONG = AbstractJpaBeanWithIdOfTypeLong.getInvalidId();

	@Override
	public boolean supports(Class<?> clazz) {
		return ItemTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BidTO bid = (BidTO) target;
		if (bid.getAmount() <= 0L) {
			errors.reject("amount", "invalid");
		}
		if (bid.getBidderId().equals(INVALID_LONG)) {
			errors.reject("bidderId", "invalid");
		}
		if (bid.getItemId().equals(INVALID_LONG)) {
			errors.reject("itemId", "invalid");
		}
	}

}
