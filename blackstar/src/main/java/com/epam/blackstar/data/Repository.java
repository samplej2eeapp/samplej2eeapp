package com.epam.blackstar.data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.epam.blackstar.data.beans.HasId;
import com.epam.blackstar.services.SearchRequest;
import com.epam.blackstar.services.SearchResult;

/**
 * Generic repository interface.
 * 
 * @param <EntityType> public interface of which JpaType implements
 * @param <EntityIdType> type of id
 */
public interface Repository<EntityType extends HasId<EntityIdType>, EntityIdType> {

	@Null
	EntityType getById(@NotNull EntityIdType id);

	void delete(@NotNull EntityType entity);

	@NotNull
	EntityType update(@NotNull EntityType entity);

	@NotNull
	EntityType create(@NotNull EntityType entity);
	
	@NotNull
	SearchResult<EntityType> search(SearchRequest<EntityType> query);

	@NotNull
	SearchResult<EntityType> jpqlSearch(String searchString);
}
