package com.epam.blackstar.data.beans;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.metamodel.Metamodel;
import javax.validation.constraints.NotNull;

/**
 * Bean with concrete id type of {@link Long}. {@link Metamodel} generation begins here.
 */
@MappedSuperclass
public class AbstractJpaBeanWithIdOfTypeLong implements HasId<Long> {
	
	@Id
	@GeneratedValue
	protected Long id;

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	@NotNull
	public static Long getInvalidId() {
		return -1L;
	}
}
