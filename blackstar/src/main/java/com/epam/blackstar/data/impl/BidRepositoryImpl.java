package com.epam.blackstar.data.impl;

import org.springframework.stereotype.Repository;

import com.epam.blackstar.data.BidRepository;
import com.epam.blackstar.data.beans.Bid;

@Repository
public class BidRepositoryImpl extends AbstractJpaRepository<Bid, Long> implements BidRepository {

	public BidRepositoryImpl() {
		super(Bid.class);
	}
}
