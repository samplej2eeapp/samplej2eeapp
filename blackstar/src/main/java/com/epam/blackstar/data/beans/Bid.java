package com.epam.blackstar.data.beans;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.epam.blackstar.util.Proxies;

/**
 * JPA bid class.
 */
@Entity
public class Bid extends AbstractJpaBeanWithIdOfTypeLong {
	
	@NotNull
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private User bidder;
	
	@NotNull
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Item item;
	
	@NotNull
	private Long amount;
	
	/**
	 * This constructor is used by JPA.
	 */
	protected Bid() {
		bidder = Proxies.generateNonFunctionalImplementation(User.class);
		item = Proxies.generateNonFunctionalImplementation(Item.class);
		amount = getInvalidId();
	}
	
	public Bid(@NotNull User bidder, @NotNull Item item, @NotNull Long amount) {
		this.bidder = bidder;
		this.item = item;
		this.amount = amount;
	}

	@NotNull
	public User getBidder() {
		return bidder;
	}

	public void setBidder(@NotNull User bidder) {
		this.bidder = bidder;
		if (!bidder.getBids().contains(this)) {
			bidder.addBid(this);
		}
	}
	
	@NotNull
	public Item getItem() {
		return item;
	}

	public void setItem(@NotNull Item item) {
		this.item = item;
		if (!item.getBids().contains(this)) {
			item.addBid(this);
		}
	}
	
	@NotNull
	public Long getAmount() {
		return amount;
	}

	public void setAmount(@NotNull Long amount) {
		this.amount = amount;
	}
}
