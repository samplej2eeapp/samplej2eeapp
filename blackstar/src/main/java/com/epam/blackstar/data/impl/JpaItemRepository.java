package com.epam.blackstar.data.impl;

import java.util.Collection;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.blackstar.data.ItemRepository;
import com.epam.blackstar.data.beans.Item;
import com.epam.blackstar.data.beans.Item_;
import com.epam.blackstar.util.Nulls;

/**
 * JPA-bound implementation of {@link ItemRepository} interface.
 */
@Repository
public class JpaItemRepository extends AbstractJpaRepository<Item, Long> implements ItemRepository {
	
	public JpaItemRepository() {
		super(Item.class);
	}

	@Override
	@Transactional
	public Collection<Item> getByIds(Collection<Long> ids) {
		CriteriaQuery<Item> query = getEntityManager().getCriteriaBuilder().createQuery(Item.class);
		Root<Item> root = query.from(Item.class);
		return Nulls.check(getEntityManager().createQuery(query.where(root.get(Item_.id).in(ids))).getResultList());
	}

}
