package com.epam.blackstar.data.impl;

import com.epam.blackstar.data.beans.User_;
import com.epam.blackstar.util.Nulls;
import org.springframework.stereotype.Repository;

import com.epam.blackstar.data.UserRepository;
import com.epam.blackstar.data.beans.User;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;

/**
 * JPA-bound implementation of {@link UserRepository} interface.
 */
@Repository
public class JpaUserRepository extends AbstractJpaRepository<User, Long> implements UserRepository {

	public JpaUserRepository() {
		super(User.class);
	}

	@Override
	@Transactional
	public Collection<User> getByIds(Collection<Long> ids) {
		CriteriaQuery<User> query = getEntityManager().getCriteriaBuilder().createQuery(User.class);
		Root<User> root = query.from(User.class);
		return Nulls.check(getEntityManager().createQuery(query.where(root.get(User_.id).in(ids))).getResultList());
	}

}
