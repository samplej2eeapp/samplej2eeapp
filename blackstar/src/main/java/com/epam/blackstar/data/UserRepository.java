package com.epam.blackstar.data;

import com.epam.blackstar.data.beans.User;

import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * User repository interface.
 */
public interface UserRepository extends Repository<User, Long> {

	@NotNull
	public abstract Collection<? extends User> getByIds(@NotNull Collection<Long> ids);

}
