package com.epam.blackstar.data.validation;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Validator;

import com.epam.blackstar.data.beans.AbstractJpaBeanWithIdOfTypeLong;
import com.epam.blackstar.services.transfer.ItemTO;

public class ItemTOValidator implements Validator {
	private Long INVALID_LONG = AbstractJpaBeanWithIdOfTypeLong.getInvalidId();
	
	@Override
	public boolean supports(Class<?> clazz) {
		return ItemTO.class.isAssignableFrom(clazz);
	}
	
	
	public void validate(Object target, org.springframework.validation.Errors errors) {
		ItemTO itemTO = (ItemTO) target;
		if (itemTO.getName().isEmpty()) {
			errors.rejectValue("name", "empty");
		}
		if (itemTO.getDescription().isEmpty()) {
			errors.rejectValue("description", "empty");
		}
		if (itemTO.getSellerId().equals(INVALID_LONG)) {
			errors.rejectValue("sellerId", "empty");
		}
	};

}
