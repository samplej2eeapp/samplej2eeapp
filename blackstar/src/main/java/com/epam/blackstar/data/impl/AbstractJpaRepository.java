package com.epam.blackstar.data.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.epam.blackstar.data.Repository;
import com.epam.blackstar.data.beans.HasId;
import com.epam.blackstar.services.SearchRequest;
import com.epam.blackstar.services.SearchResult;
import com.epam.blackstar.services.impl.GenericSearchResult;
import com.epam.blackstar.util.Nulls;

/**
 * Generic implementation of {@link Repository}.
 * 
 * @param <JpaType> JPA-annotated type
 * @param <IdType> type of id
 */
public abstract class AbstractJpaRepository<JpaType extends HasId<IdType>, IdType> implements Repository<JpaType, IdType> {

	private EntityManager em;
	private Class<JpaType> targetClass;

	public AbstractJpaRepository(Class<JpaType> targetClass) {
		this.targetClass = targetClass;
	}

	@Override
	@Transactional
	public JpaType getById(IdType id) {
		return getEntityManager().find(targetClass, id);
	}
	
	@Override
	@Transactional
	public void delete(JpaType entity) {
		getEntityManager().remove(entity);
	}

	@Override
	@Transactional
	public JpaType update(JpaType entity) {
		Assert.notNull(entity.getId());
		
		return getEntityManager().merge(entity);
	}

	@Override
	@Transactional
	public JpaType create(JpaType entity) {
		getEntityManager().persist(entity);
		return entity;
	}
	
	@Override
	public SearchResult<JpaType> search(SearchRequest<JpaType> query) {
		CriteriaQuery<JpaType> criteriaQuery = getEntityManager().getCriteriaBuilder().createQuery(targetClass);
		criteriaQuery.from(targetClass);
		
		TypedQuery<JpaType> typedQeury = getEntityManager().createQuery(criteriaQuery);
		typedQeury.setFirstResult(query.getOffset().intValue());
		typedQeury.setMaxResults(query.getCount().intValue());
		List<JpaType> items = typedQeury.getResultList();
		return new GenericSearchResult<>(Nulls.check(items));
	}
	
	@Override
	public SearchResult<JpaType> jpqlSearch(String searchString) {
		TypedQuery<JpaType> query = em.createQuery(searchString, targetClass);
		return new GenericSearchResult<>(Nulls.check(query.getResultList()));
	}

	protected EntityManager getEntityManager() {
		if (em == null)
			throw new IllegalStateException("EntityManager is null");
		return em;
	}

	@PersistenceContext
	protected void setEntityManager(EntityManager em) {
		this.em = em;
	}
}
