package com.epam.blackstar.data.beans;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.epam.blackstar.util.Proxies;

/**
 * JPA item class.
 */
@Entity
public class Item extends AbstractJpaBeanWithIdOfTypeLong {

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@NotNull
	private User seller;
	
	@NotNull
	private String name = "";
	
	@NotNull
	private String description = "";
	
	@NotNull
	@OneToMany(mappedBy = "item", targetEntity = Bid.class, fetch = FetchType.EAGER )
	private Set<Bid> bids = new HashSet<>();
	
	/*
	 * This constructor is used by JPA.
	 */
	protected Item() {
		seller = Proxies.generateNonFunctionalImplementation(User.class);
	}

	public Item(@NotNull User seller) {
		this.seller = seller;
	}

	@NotNull
	public User getSeller() {
		return seller;
	}

	public void setSeller(@NotNull User user) {
		seller = user;
		if (!user.getItems().contains(this)) {
			user.addItem(this);
		}
	}

	@NotNull
	public String getName() {
		return name;
	}

	public void setName(@NotNull String name) {
		this.name = name;
	}

	@NotNull
	public String getDescription() {
		return description;
	}

	public void setDescription(@NotNull String description) {
		this.description = description;
	}

	@NotNull
	public Set<Bid> getBids() {
		return bids;
	}

	public void setBids(@NotNull Set<Bid> bids) {
		this.bids = bids;
	}
	
	public void addBid(Bid bid) {
		if (!bids.contains(bid)) {
			bids.add(bid);
		}
		if (bid.getItem() != this) {
			bid.setItem(this);
		}
	}
}
