package com.epam.blackstar.data;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.blackstar.services.ItemService;
import com.epam.blackstar.services.UserService;
import com.epam.blackstar.services.transfer.ItemTO;
import com.epam.blackstar.services.transfer.UserTO;
import com.epam.blackstar.services.transfer.impl.ItemTOImpl;
import com.epam.blackstar.services.transfer.impl.UserTOImpl;

@Component
public class SampleDataGenerationHelper {

	private volatile boolean sampleDataAdded = false;
	private ItemService itemService;
	private UserService userService;

	@Autowired
	public SampleDataGenerationHelper(ItemService itemService, UserService userService) {
		this.itemService = itemService;
		this.userService = userService;
	}

	public synchronized void generateSampleData() {
		if (sampleDataAdded)
			return;

		UserTO user = new UserTOImpl();
		int userCount = 1;
		int itemCount = 1;
		while (userCount < 10) {
			user.setName("User " + userCount++);
			Long sellerId = userService.create(user);
			while (itemCount < 5 * userCount) {
				ItemTO item = new ItemTOImpl();
				item.setSellerId(sellerId);
				item.setName("Item " + itemCount++);
				itemService.create(item);
			}
		}
		sampleDataAdded = true;
	}
	
	@NotNull
	public synchronized Boolean canAddSampleData() {
		return !sampleDataAdded;
	}
	
}
