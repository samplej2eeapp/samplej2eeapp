package com.epam.blackstar.data.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.epam.blackstar.services.transfer.UserTO;
import com.google.common.base.Strings;

public class UserTOValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return UserTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		UserTO user = (UserTO) target;
		if (Strings.isNullOrEmpty(user.getLogin())) {
			errors.rejectValue("login", "empty", "empty login");
		}
		if (Strings.isNullOrEmpty(user.getName())) {
			errors.rejectValue("name", "empty", "empty name");
		}
		if (Strings.isNullOrEmpty(user.getEmail())) {
			errors.rejectValue("email", "empty", "empty email");
		}
		if (Strings.isNullOrEmpty(user.getBillingAddress())) {
			errors.rejectValue("billingAddress", "empty", "empty billing address");
		}
		if (Strings.isNullOrEmpty(user.getEmail())) {
			errors.rejectValue("password", "empty", "empty password");
		}
		if (!user.getEmail().matches("^.*@.*$")) {
			errors.rejectValue("email", "invalid", "invalid email");
		}
	}

}
