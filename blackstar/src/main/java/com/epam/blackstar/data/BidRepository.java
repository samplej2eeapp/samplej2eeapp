package com.epam.blackstar.data;

import com.epam.blackstar.data.beans.Bid;

public interface BidRepository extends Repository<Bid, Long> {
}
