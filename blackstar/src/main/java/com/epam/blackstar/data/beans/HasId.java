package com.epam.blackstar.data.beans;


/**
 * Supports Id property.
 * 
 * @param <T> type of Id property.
 */
public interface HasId<T> {

	void setId(T id);

	T getId();
}
