package com.epam.blackstar.data.validation;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Component
public class GlobalValidator implements Validator {

	private Map<Class<?>, Validator> validatorsCache = Maps.newHashMap();
	private Collection<Validator> validators = Lists.newArrayList();
	
	public GlobalValidator() {
		registerValidators();
	}

	private void registerValidators() {
		Validator[] allValidators = new Validator[] {
			new UserTOValidator(),
			new ItemTOValidator(),
			new BidTOValidator()
			
		};
		
		validators.addAll(Arrays.asList(allValidators));
	}

	@Override
	public boolean supports(Class<?> clazz) {
		for (Validator v : validators) {
			if (v.supports(clazz)) {
				validatorsCache.put(clazz, v);
				return true;
			}
		}
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(validatorsCache.get(target.getClass()) != null)
			validatorsCache.get(target.getClass()).validate(target, errors);
	}

}
