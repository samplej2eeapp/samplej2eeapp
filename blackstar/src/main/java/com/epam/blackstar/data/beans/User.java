package com.epam.blackstar.data.beans;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

/**
 * JPA user class.
 */
@Entity
public class User extends AbstractJpaBeanWithIdOfTypeLong {
	
	@NotNull
	@OneToMany(mappedBy = "seller", targetEntity = Item.class, fetch = FetchType.EAGER )
	private Set<Item> items = new HashSet<>();
	
	@NotNull
	@OneToMany(mappedBy = "bidder", targetEntity = Bid.class, fetch = FetchType.EAGER )
	private Set<Bid> bids = new HashSet<>();
	
	@NotNull
	private String name = "";
	
	@NotNull
	private String billingAddress = "";
	
	@NotNull
	private String password = "";
	
	@NotNull
	private String login = "";
	
	@NotNull
	private String email = "";

	@NotNull
	public Set<Item> getItems() {
		return items;
	}
	
	public void setItems(@NotNull Set<Item> items) {
		this.items = items;
	}

	public void addItem(@NotNull Item item) {
		items.add(item);
		if (item.getSeller() != this) {
			item.setSeller(this);
		}
	}
	
	public void addBid(@NotNull Bid bid) {
		bids.add(bid);
		if (bid.getBidder() != this) {
			bid.setBidder(this);
		}
	}

	@NotNull
	public String getEmail() {
		return email;
	}

	public void setEmail(@NotNull String email) {
		this.email = email;
	}
	
	@NotNull
	public String getLogin() {
		return login;
	}

	public void setLogin(@NotNull String login) {
		this.login = login;
	}
	
	@NotNull
	public String getName() {
		return name;
	}

	public void setName(@NotNull String name) {
		this.name = name;
	}

	@NotNull
	public Set<Bid> getBids() {
		return bids;
	}

	public void setBids(@NotNull Set<Bid> bids) {
		this.bids = bids;
	}
	
	@NotNull
	public String getPassword() {
		return password;
	}

	public void setPassword(@NotNull String password) {
		this.password = password;
	}
	
	@NotNull
	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(@NotNull String billingAddress) {
		this.billingAddress = billingAddress;
	}
}
